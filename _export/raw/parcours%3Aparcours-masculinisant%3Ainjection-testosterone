Dans le cadre du parcours libre et consenti, on peut préférer être autonome le plus possible dans la prise de son traitement. Cela est tout à fait possible pour les injections de [[medicaments:testosterone|testostérone]] en respectant hygiène et rigueur.

Vous trouverez aussi un excellent tutoriel à ce sujet sur le blog de //[[http://imnotacisboy.blogspot.fr/2015/03/tuto-injection-dandrotardyl-fesse.html|I'm not a cisboy]]// ou [[https://www.youtube.com/watch?v=7agxDdFJcwg|en vidéo sur YouTube]].

Notez que l'injection intramusculaire est désormais préconisée par l'OMS dans la moitié haute extérieure de la cuisse. Vous pouvez néanmoins le faire à l'épaule ou la fesse selon ce que vous trouvez le plus pratique.

----

===== Tutoriel d'injection de testostérone dans la cuisse =====

Nous proposons aussi un tutoriel pour l'injection, celui-ci se base sur un extrait de La revue //Prescrire// {{parcours:parcours-masculinisant:prescrire-320_injections-intramusculaires.pdf|de juin 2010}} (Tome 320, pages 433 à 437).

La suite de la page sera donc en partie constituée d'extraits, synthétisant la source pour la rendre plus accessible.

<note important>L’injection intramusculaire est utilisée pour administrer de nombreux médicaments et vaccins.

**Elle expose à certains effets indésirables spécifiques :** lésion d’un nerf périphérique, hématome, abcès, gangrène, contracture musculaire et fibrose, dermatite livédoïde.</note>

==== Préparation de l’injection ====

« Le risque infectieux est minimisé en n’utilisant que des seringues et aiguilles stériles, si possible à usage unique. »

Notez qu'en cas de partage de votre dose de testostérone, vous pouvez utiliser la même seringue mais pas les mêmes aiguilles.
Excepté en cas de test du retour veineux où vous devrez aussi changer de seringue, car potentiellement souillée avec l'aspiration (voir § [[parcours:parcours-masculinisant:injection-testosterone#l_injection|L’injection]]).

Dans tous les cas, changez de matériel à chaque nouvelle préparation d'injection.

Nous vous conseillons des seringues de 2 mL.

Le diamètre des pistons des seringues de 5 mL étant plus large, la force à produire pour l'actionner se retrouve diffusée sur une plus grande surface, donc moins concentrée, obligeant à fournir une pression plus forte.

Aussi il est d'usage d'utiliser deux aiguilles différentes :
  * une grosse (réf : 18G) pour recueillir la solution dans l'ampoule car il y a un risque d'émousser le bout de l'aiguille, le diamètre large permet un plus gros débit ;
  * une plus petite (réf : 22G) pour l'injection intramusculaire.

Comme toujours avec le matériel médical, vérifiez les dates de péremption indiquées.

Préparez une surface plane et stable, comme une table ou un plateau.

Sortez l'ampoule d'[[medicaments:testosterone|Androtardyl]] de son carton.

[[http://sante.gouv.qc.ca/conseils-et-prevention/lavage-des-mains/|Lavez vous soigneusement les mains.]]

Désinfectez avec attention votre plan de travail puis placez vos aiguilles et seringue encore ensachées dessus, ainsi que votre fiole de testostérone.\\
Ne mettez pas votre matériel de désinfection à la poubelle afin d'éviter de vous relaver les mains.

Il faut désormais créer une bulle d'air dans le haut de votre ampoule.

Pour cela vous pouvez soit :
  * la prendre bien en main et donner un grand coup sec vers le bas ;
  * la garder quelques instants sous l'aisselle ou à l'aine, entre vos cuisses ;
  * frotter énergiquement son cul sur une surface plane, comme une table.

Désormais tenez la fiole avec le point bleu tourné vers vous, prenez du coton pour protéger vos doigts et maintenez la partie sécable avec votre pouce sur le point bleu et l'index derrière.

Puis poussez vers l'arrière pour casser le verre.

Vous pouvez maintenant ouvrir l'emballage de votre seringue et de la grosse aiguille (18G) pour ensuite les assembler en prenant soin de garder les sachets pour la suite.

Aspirez doucement le contenu de votre ampoule jusqu'à votre dosage (et un peu d'air) et remettez l'étui autour de l'aiguille puis chassez le plus gros des bulles en tapotant doucement sur la seringue.

Retirez votre première aiguille et remettez-la dans son emballage pour ne pas la perdre. Placez la seconde sur la seringue sans enlever son étui.

==== Préparation de la zone d’injection ====

Le document de la revue //Prescrire// (page 436) [[http://www.who.int/bulletin/volumes/81/7/Hutin0703.pdf|et l'OMS]] indiquent que l'utilité de la désinfection cutanée est controversée, néanmoins elle reste utile dans un schéma de rituel permettant de décompresser. Il est aussi indiqué que des exercices de respiration ont fait leurs preuves pour se calmer.

{{ :parcours:parcours-masculinisant:zone-injection.png?nolink&600|}}

L'OMS conseille d'injecter dans le [[wpfr>Muscle_vaste_latéral|muscle vaste externe]], nous nous concentrerons donc sur celui-ci dans notre guide.

Vous trouverez sur l'image ci-contre un dessin vous indiquant où se trouve la zone d'injection.

Dégagez la zone d'injection et désinfectez si nécessaire. Vous pouvez vous aider en marquant un point avec un stylo.

==== L’injection ====

Prenez votre seringue, expulsez le restant d'air jusqu'à faire perler une goutte de la solution.

Insérez plutôt rapidement l'aiguille avec un angle de 45° en orientant le haut du piston vers votre genou. \\
Lorsque l'aiguille "butte" dans les fibres du muscle, il faut suivre le mouvement, car l'aiguille va naturellement s'incliner un peu plus vers le bas.

« Aspirer avant d’injecter lentement. Une fois l’aiguille insérée dans le muscle et avant d’injecter, mieux vaut faire une aspiration en exerçant une  traction sur le piston de la seringue. Si un reflux sanguin survient, cela signifie que l’extrémité de l’aiguille se trouve dans un gros vaisseau. Il faut alors s’abstenir d’injecter. »

Attendez 5 à 10 secondes puis retirez l'aiguille.

Vous pouvez optionnellement masser un peu, vous verrez avec l'habitude si cela amenuise la douleur ou non pour vous.

Mettez un pansement mignon. 😘

==== Nettoyage de la zone de travail ====

Remettez l'étui sur votre seconde aiguille puis retirez-la de la seringue.

Jetez vos deux aiguilles dans un conteneur de récupération, que vous pourrez [[https://www.dastri.fr/|trouver en pharmacie]]. Vous pouvez mettre votre fiole d'Androtardyl avec.

Les seringues peuvent aller dans une poubelle classique, tout comme les compresses et les emballages plastiques des aiguilles.

Pensez à suivre votre dosage hormonal en faisant des [[parcours:documents:ordonnance|prises de sang]] régulières. Vous pouvez vous aider de notre [[parcours:documents:analysessanguines|tableau de valeurs hormonales]] pour comparer vos taux.