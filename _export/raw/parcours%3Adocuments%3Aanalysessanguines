==== Tableau de valeurs hormonales de références =====

Celui-ci a été obtenu par croisement de plusieurs valeurs de laboratoires.

**Ces données sont indicatives.** Il reste important d'avoir un suivi médical (médecin traitant ou "spécialiste").

Nous vous proposons [[parcours:documents:ordonnance|une ordonnance]] présentant tout ce sur quoi il est important de garder un œil (hormones, reins, foie).

Pour les femmes trans, nous avons dû sélectionner des fourchettes de valeur lors des [[wpfr>Cycle_menstruel|phases menstruelles]].

----

Vous pouvez comparer vos dosages en regardant les résultats du laboratoire et ceux des colonnes de ce tableau.

Pour les hommes trans, vous référer aux colonnes "Homme", et "Femme" pour les femmes trans.

^ ^ Homme ^^ Femme ^^
| | min | max | min | max |
| FSH (UI/L) ^ 1,5 ^ 12,4 ^ 1,7 ^ 21,5 ^
| LH (UI/L) ^ 1,7 ^ 8,6 ^ 1 ^ 95,6 ^
| Œstradiol (pg/mL) ^ 9 ^ 62 ^ 12,4 ^ 398 ^
| Progestérone (ng/mL) ^ 0,05 ^ 0,149 ^ 0,057 ^ 23,9 ^
| Prolactine (ng/mL) ^ 4,04 ^ 15,2 ^ 4,79 ^ 23,3 ^
| Testostérone Totale (ng/mL) ^ 3 ^ 10 ^ 0,2 ^ 0,8 ^
| Testostérone Libre Calculée (pg/L) ^ 70 ^ 200 ^ 2,6 ^ 8,1 ^
| Testostérone Biodisponible Calculée (µg/L) ^ 1,8 ^ 3,5 ^ ^ ^
| DHT (androstanolone) (ng/mL) ^  ^  ^ 0,75 ^ 3,89 ^

----

Ce second tableau est ici pour indiquer quelles valeurs ont étés retenues pour le tableau final, visible ci-dessus.


^ Valeurs hormonales du cycle menstruel | Phase folliculaire || Pic ovulatoire || Phase lutéale || Ménopause ||
| | min | max | min | max | min | max | min | max |
| FSH (UI/L) | 3,5 | 12,5 | 4,7 ^ 21,5 ^ 1,7 | 7,7 | 25,8 | 134,8 |
| LH (UI/L) | 2,4 | 12,6 | 14 ^ 95,6 ^ 1 | 11,4 | 7,7 | 58,5 | 
| Œstradiol (pg/mL) ^ 12,4 | 233 | 41 ^ 398 | 22,3 | 341 | | 51,6 |
| Progestérone (ng/mL) ^ 0,057 | 0,893 | 0,121 | 12 | 1,83 ^ 23,9 | 0,05 | 0,126 |
| Prolactine (ng/mL) ^ 4,79 ^ 23,3 ^ 4,79 ^ 23,3 ^ 4,79 ^ 23,3 ^ 4,79 ^ 23,3 ^