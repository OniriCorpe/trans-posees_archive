<?xml version="1.0"?>
<OpenSearchDescription xmlns="http://a9.com/-/spec/opensearch/1.1/">
  <ShortName>Trans Posé⋅e⋅s</ShortName>
  <Image width="16" height="16" type="image/x-icon">https://xn--transposes-i7a.eu/favicon.ico</Image>
  <Url type="text/html" template="https://xn--transposes-i7a.eu/doku.php?do=search&amp;id={searchTerms}" />
  <Url type="application/x-suggestions+json" template="https://xn--transposes-i7a.eu/lib/exe/ajax.php?call=suggestions&amp;q={searchTerms}" />
</OpenSearchDescription>
